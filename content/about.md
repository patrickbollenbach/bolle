---
title: About Patrick
date: 2018-06-05T14:29:45.000+00:00
layout: single
type: page

---
Hello! I'm Patrick. I've been a freelance Shopify developer for about 5 years now.

I've travelled around the world helping clients (both big and small) build unique e-commerce sites and solutions on the Shopify platform.

These days I'm focused on Shopify app development. My main two projects are Recipe Kit & Wait.li, both (relatively) popular public apps on the Shopify app store.

While most of my time is spent improving my apps, I'm still keen on helping businesses out when I have spare time. If you have an interesting problem to solve on Shopify, or just want to chat - send me an email and I'll get in touch!

Thanks!