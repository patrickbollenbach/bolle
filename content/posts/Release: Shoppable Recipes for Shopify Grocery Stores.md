---
post_title: 'Release: Shoppable Recipes for Shopify Grocery Stores '
title: Shoppable Recipe Cards for Shopify Grocery Stores
date: 2021-03-23T04:00:00.000+00:00

---
I've been working hard releasing plenty of new features onto my recent (well, 1 year old) Shopify app called [**Recipe Kit**.](https://apps.shopify.com/recipe-kit?source=bolle_website "Link to Recipe Kit app listing")

It has been an incredible year working with now almost 1000 different Shopify stores, building features and enabling their companies to post high quality recipe cards onto their blog.

By far the #1 request I've received for app features is to implement an 'add to cart' function into the app's recipes. I've since realized that this is analogous to the popular 'shoppable recipe' craze hitting the e-commerce world since COVID hit last year.

After months of working and testing (with some lovely merchants help), I've finally just released the shoppable recipe card feature into Recipe Kit.

It's hard to explain how excited this feature makes the users of my app, and I. It seems this is the perfect culmination of technology and useful content for internet users. 

In one go, a store owner can create:

* Useful, delicious content in a recipe card that readers can reference over and over again
* And at the same time, a sales / lead generating page which encourages purchases of their store's products

I'm not an e-com marketing expert, nor have ever claimed to be. I'm a developer that builds tools that allow store owners to do their best work (building and selling their products). But, in saying all of this, these shoppable recipes really feel like they are on the leading edge of web content / technology. It is super exciting.

Companies like Walmart (and Tasty) have already figured this out, enabling shoppable recipe cards on the Tasty website / app. 

I think this feature is really great for both the store owner and the recipe reader; it's a win-win in my eyes.

Check out my blog post on the Recipe Kit website that goes on about how this works on Shopify here: [https://recipekit.app/blogs/guides/shoppable-recipes-are-here-for-shopify](https://recipekit.app/blogs/guides/shoppable-recipes-are-here-for-shopify "https://recipekit.app/blogs/guides/shoppable-recipes-are-here-for-shopify")