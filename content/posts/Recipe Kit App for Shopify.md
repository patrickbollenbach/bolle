---
post_title: 'Shopify App Launch: Recipe Kit, an easy recipe builder for Shopify stores. '
title: 'App Launch: Recipe Kit, an easy recipe builder for Shopify stores.'
date: '2020-05-28T16:14:17.000+00:00'

---
[![Recipe Kit Shopify App Listing Page](/uploads/screenshot_2020-06-16-recipe-kit-ecommerce-plugins-for-online-stores-shopify-app-store.png "Recipe Kit Shopify App Listing")](https://apps.shopify.com/recipe-kit?source=bolle_website)

I've been focusing on building Shopify apps ever since I launched my first app called [Wait.li ](https://apps.shopify.com/product-waiting-lists "Product Waiting Lists for Shopify stores")- which is a product waiting list app for Shopify stores looking to launch new products.

I've had the privilege of working with various Shopify store owners over the past 4 years of freelancing all over the world, and recently found a gap in the app store marketplace. There was simply no way to write and post good looking recipes on your Shopify store.

Wordpress has always had hundreds of recipe plugins, of course, but somehow nobody had built a recipe app specifically built in with Shopify that integrates with pages, blog posts, products, etc.

So, I began an adventure to build a recipe app for Shopify.

### **From idea to app launch, the entire process took me about 5 weeks.**

This was super quick due to the fact that I was able to re-use a good chunk of code from Wait.li, thankfully. I also worked with a fantastic app reviewer (thanks Ivana!) who responded super quickly and gave me great advice.

So... here it is:

## Recipe Kit

App Store URL: [https://apps.shopify.com/recipe-kit](https://apps.shopify.com/recipe-kit?source=bolle_website "Recipe Kit recipe card builder for Shopify")  
Marketing / documentation URL: [https://recipekit.app/](https://recipekit.app/ "https://recipekit.app/")  
Demo URL: [https://demo.recipekit.app/blogs/recipes/my-favourite-mac-cheese-recipe](https://demo.recipekit.app/blogs/recipes/my-favourite-mac-cheese-recipe "https://demo.recipekit.app/blogs/recipes/my-favourite-mac-cheese-recipe")

### What does it do?

Basically, Recipe Kit allows Shopify store owners to super easily create great looking, SEO optimized, product embedded recipe cards to show on their store's blog posts.

### Some features

* Publish high quality recipes to attract new customers to your store.
* Built in Google SEO optimized recipe cards so your recipes display beautifully in Google results.
* Connect your Shopify store products to recipe ingredients for easy purchasing by customers.
* Your recipe, your design. Change colours, borders, and layout without any hassle.
* Recipe Kit currently comes with 2 modern recipe designs, but we are releasing new designs often! Have a recommendation? Send it over and we will build it out.
* Easy to print for customers that like the old-fashioned approach!
* Instantly share-able to Facebook, Pinterest, and Twitter.
* NEW: Translate recipe card language super easily on the App Settings page!
* NEW: Recipe star ratings now available! Recipe ratings display on Google searches and show great social proof to new customers / readers.

### Have a question? Feature request?

Send me a message via help@recipekit.app or patrick@recipekit.app and I'll get back to you and work on an update ASAP to fit your business needs :)